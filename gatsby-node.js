const path = require('path')

exports.createPages = async ({ graphql, actions }) => {

  const { data } = await graphql(`
    query Articles {
      allMarkdownRemark {
        nodes {
          frontmatter {
            slug
          }
        }
      }
    }
  `)

  data.allMarkdownRemark.nodes.forEach(node => {
    const slug = node.frontmatter.slug
    actions.createPage({
      path: '/projects/'+ slug,
      component: require.resolve('./src/templates/project-details.js'),
      context: { slug: slug }
    })
  })

}



// const path = require("path")
// exports.createPages = async ({ graphql, actions, reporter }) => {
//     const { createPage } = actions

//       const result = await graphql(`
//     query Articles {
//       allMarkdownRemark {
//         nodes {
//           frontmatter {
//             slug
//           }
//         }
//       }
//     }
//   `)

//       const blogPostTemplate = path.resolve('src/templates/project-details.js')
//   result.allMarkdownRemark.nodes.forEach(({ node }) => {
//     // const path = '/projects/'+ node.frontmatter.slug
//     createPage({
//       path: '/projects/'+ node.frontmatter.slug,
//       component: blogPostTemplate,
//       // In your blog post template's graphql query, you can use pagePath
//       // as a GraphQL variable to query for data from the markdown file.
//       context: { slug: node.frontmatter.slug },
//     })
//   })

// }


// const path = require("path")
// exports.createPages = async ({ graphql, actions }) => {
//   const { createPage } = actions
//   const queryResults = await graphql(`
//   query Articles {
//     allMarkdownRemark {
//       nodes {
//         frontmatter {
//           slug
//         }
//       }
//     }
//   }
//   `)
//   const productTemplate = path.resolve('./src/templates/project-details.js')
//   queryResults.allMarkdownRemark.nodes.forEach(node => {
//     createPage({
//       path: `/products/${node.slug}`,
//       component: productTemplate,
//       context: {
//         // This time the entire product is passed down as context
//         product: { slug: node.frontmatter.slug },
//       },
//     })
//   })
// }