import { graphql, Link } from "gatsby"
import React from "react"
import Layout from "../components/Layout"
import * as styles from '../styles/home.module.css'
// import Img from "gatsby-image"
import { GatsbyImage, getImage } from "gatsby-plugin-image";

export default function Home({ data }) {
  console.log(data)
  const image = getImage(data.file.childImageSharp.gatsbyImageData)
  console.log(image)
  return (
    <Layout>
      <section className={styles.header}>
      <div>
        <h2> Design </h2>
        <h3> Develop & Deploy </h3>
        <p> Ux designer kjdfhgklsdhf kudyhfiejh </p>
        <Link className={styles.btn} to="/projects" > My Projects </Link>
      </div>
      {/* <img src="/banner.png" alt="site" style={{ maxWidth: '100%'}}></img> */}
      {/* <Img fluid={ data.file.childrenImageSharp.fluid } /> */}
      <GatsbyImage image={image} alt="Banner" />
    </section>
    </Layout>
    
  )
}


export const query = graphql`
query Banner {
  file(relativePath: {eq: "banner.png"}) {
    childImageSharp {
      gatsbyImageData(
        layout: FULL_WIDTH
        placeholder: BLURRED
        formats: [AUTO, WEBP]
        )
    }
  }
}
`